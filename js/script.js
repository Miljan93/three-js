var camera, scene, renderer,
geometry, material, mesh, controls, lopta;

init();
animate();

function init() {

    scene = new THREE.Scene();
    scene.name = "Basketball";
    camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.name = "Kamera";
    camera.position.set(0, 300, 1000);
    scene.add( camera );

   
    objects();
    sceneobjects();
    lights();
    

    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize( window.innerWidth, window.innerHeight);
    window.addEventListener("resize", function rsz(){
        renderer.setSize( window.innerWidth, window.innerHeight);
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
    });
    renderer.setClearColor(0x95a5a6);
	renderer.setPixelRatio(window.devicePixelRatio);
    renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.PCFShadowMap; // default THREE.PCFShadowMap
    document.body.appendChild( renderer.domElement ); 

    controls = new THREE.OrbitControls(camera, renderer.domElement);
}
function objects() {

    var objectLoader = new THREE.JSONLoader();
    material1 = new THREE.MeshPhongMaterial( { map: new THREE.TextureLoader().load("models/NBA BASKETBALL DIFFUSE.jpg"), morphTargets: true} );
	objectLoader.load("models/Ball.json", function dodaj( obj ) {
	lopta = new THREE.Mesh(obj, material1);
	lopta.name = "Lopta";
	lopta.scale.set(4,4,4);
	lopta.position.set(0,200,0); 
	lopta.castShadow = true;
		scene.add( lopta);	
	});
}

function sceneobjects() {
	
	geometry = new THREE.PlaneGeometry( 3000, 5000);
    material = new THREE.MeshPhongMaterial( { map: new THREE.TextureLoader().load("textures/Court.png") , shininess: 100} );
    material.side = THREE.DoubleSide;
    pod = new THREE.Mesh( geometry, material );
    pod.name = "Teren";
    pod.position.set(0, 0, 0);
    pod.rotateX(-90 * Math.PI / 180);
    pod.receiveShadow = true;
    scene.add( pod );
}

function lights() {
	var light = new THREE.SpotLight( 0xffffff, 2.0, 4000 );
	light.name = "Svetlo";
	light.castShadow = true;	// default false
	light.position.set(0, 2000, -2500);           
	//Set up shadow properties for the light
	//light.shadow = new THREE.LightShadow(new THREE.PerspectiveCamera(100, 1, 500, 1000));
	light.shadow.bias = 0.0001;
	light.shadow.mapSize.width = 2048 * 2;  // default
	light.shadow.mapSize.height = 2048 * 2; // default
	scene.add( light );

	var light1 = new THREE.SpotLight( 0xffffff, 1.0, 4000 );
	light1.name = "Svetlo1";
	light1.castShadow = true;	// default false
	light1.position.set(0, 2000, 0);           
	//Set up shadow properties for the light
	//light.shadow = new THREE.LightShadow(new THREE.PerspectiveCamera(100, 1, 500, 1000));
	light1.shadow.bias = 0.0001;
	light1.shadow.mapSize.width = 2048 * 2;  // default
	light1.shadow.mapSize.height = 2048 * 2; // default
	scene.add( light1 );

	var light2 = new THREE.SpotLight( 0xffffff, 2.0, 4000 );
	light2.name = "Svetlo2";
	light2.castShadow = true;	// default false
	light2.position.set(0, 2000, 2500);           
	//Set up shadow properties for the light
	//light.shadow = new THREE.LightShadow(new THREE.PerspectiveCamera(100, 1, 500, 1000));
	light2.shadow.bias = 0.0001;
	light2.shadow.mapSize.width = 2048 * 2;  // default
	light2.shadow.mapSize.height = 2048 * 2; // default
	scene.add( light2 );
}


function animate() {
    // note: three.js includes requestAnimationFrame shim
    requestAnimationFrame( animate );
    render();
	}
	var smer = "down";
	function render() {
		/*if(lopta.position.y >= 70 && smer == "down"){
			lopta.position.y -= 5;
			lopta.rotateX(5 * Math.PI / 180);
			lopta.rotateY(2 * Math.PI / 180);
			if(lopta.position.y < 70){
				smer = "up";
			}
		}
		else if(lopta.position.y <= 600 && smer == "up"){
			lopta.position.y += 5;
			lopta.rotateX(5 * Math.PI / 180);
			lopta.rotateY(-2 * Math.PI / 180);
			if(lopta.position.y > 600){
				smer = "down";
			}
		}*/


	    renderer.render( scene, camera );
	}